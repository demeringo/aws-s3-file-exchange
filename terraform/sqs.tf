resource "aws_sqs_queue" "upload1" {
  name = var.sqs_queue_1_name
}

resource "aws_sqs_queue" "upload2" {
  name = var.sqs_queue_2_name
}

resource "aws_sns_topic_subscription" "sqs1" {
  topic_arn = aws_sns_topic.upload.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.upload1.arn
}

resource "aws_sns_topic_subscription" "sqs2" {
  topic_arn = aws_sns_topic.upload.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.upload2.arn
}

resource "aws_sqs_queue_policy" "test1" {
  queue_url = aws_sqs_queue.upload1.id
  policy    = data.aws_iam_policy_document.sqs_upload_queue1.json
}

resource "aws_sqs_queue_policy" "test2" {
  queue_url = aws_sqs_queue.upload2.id
  policy    = data.aws_iam_policy_document.sqs_upload_queue2.json
}

data "aws_iam_policy_document" "sqs_upload_queue1" {
  policy_id = "__default_policy_ID"
  statement {
    actions = [
      "sqs:SendMessage",
    ]
    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [
        aws_sns_topic.upload.arn,
      ]
    }
    effect = "Allow"
    principals {
      type = "AWS"
      identifiers = [
        "*",
      ]
    }
    resources = [
      aws_sqs_queue.upload1.arn,
    ]
    sid = "__default_statement_ID"
  }
}

data "aws_iam_policy_document" "sqs_upload_queue2" {
  policy_id = "__default_policy_ID"
  statement {
    actions = [
      "sqs:SendMessage",
    ]
    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [
        aws_sns_topic.upload.arn,
      ]
    }
    effect = "Allow"
    principals {
      type = "AWS"
      identifiers = [
        "*",
      ]
    }
    resources = [
      aws_sqs_queue.upload2.arn,
    ]
    sid = "__default_statement_ID"
  }
}

