resource "aws_s3_bucket" "upload" {
  bucket        = var.aws_s3_bucket_upload_name
  acl           = "public-read"
  force_destroy = true

  tags = {
    Name = "s3-notification-test"
  }
}

resource "aws_s3_bucket_notification" "upload" {
  bucket = aws_s3_bucket.upload.id

  topic {
    topic_arn     = aws_sns_topic.upload.arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".zip"
  }
}

