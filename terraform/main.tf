provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file = var.aws_credentials_path
  profile                 = "labiam"
}

