variable "aws_credentials_path" {
  description = "Aws credentials file to keep local to your machine"
  default     = "/home/olivier/.aws/credentials"
  type        = string
}

variable "aws_s3_bucket_upload_name" {
  default = "kribio-sns-sqs-upload-bucket-test"
}

variable "sqs_queue_1_name" {
  default = "sns-sqs-queue-1"
}

variable "sqs_queue_2_name" {
  default = "sns-sqs-queue-2"
}

