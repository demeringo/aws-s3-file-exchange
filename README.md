# File exchange demo

This is a code sample of a system that propagate data between a producer and several consumers.
Written for aws, using terraform to create the resources.

We have one producers that creates a data file (zip) at regular intervals. We want to propagate this single file to several different consumers (like a dev, test and production environments) asynchronously.

## What it does

An EC2 instance publishes files to *multiple* EC2 consumers( Fan-out scenario).

The data to transfer is too heavy to be stored directly as an SQS message, we use S3 for storage and file sharing.

1. The producer uploads the file to s3
2. On file upload, S3 sends notification message to each consumer queue (adds the message to the SQS FIFO queue of each consumer)
3. Consumer receive notification and retrieve the file from S3 asynchronously.

![Architecture view](S3-upload-to-sqs.png)

- Bucket name: `kribio-sns-sqs-upload-bucket-test`
- Queues are named `sns-sqs-queue-1` and `sns-sqs-queue-2`.

This architecture can easily be scaled to several consumers, or extended to use the SNS notification to trigger a serverless function (aws lambda).

## Disclaimer - use at your own risks !

This is demo code to show how we can use S3, SQS and SNS together, there is *plenty* of room for improvement.
Do **not use it in production**, without hardening security policies and error handling first. Specifically, the S3 bucket is configured as read write for anybody.

## Usage

### Create infrastructure

Adapt terraform variables (queue names and credentials for your account) and choose a unique S3 bucket name in `terraform/vars.tf`.

```bash
# Create infrastructure with terraform
cd terraform
terraform init
terraform plan
terraform apply
```

### Test producer: publish a data file to S3 storage

Upload a local file to trigger notification.

```bash
cd sqs_producer
# show usage
python upload_to_s3.py -h

# upload a local file to s3 bucket
python upload_to_s3.py ~/Desktop/1sample_file.zip kribio-sns-sqs-upload-bucket-test
```

### Test client: poll queue sand retrieve data

Run polling.py, passing the name of the queues as a positional argument.

```bash
cd sqs_consumer
# show usage
python polling.py -h

# Poll Queue 1 and donwload file to /tmp
python polling.py sns-sqs-queue-1

# Poll Queue 2 and donwload file to /tmp
python polling.py sns-sqs-queue-2

# Check resulting files in tmp
ls -al /tmp/sns*.zip
-rw-r--r-- 1 olivier olivier 0 Jan 21 10:51 /tmp/sns-sqs-queue-1_1sample_file.zip
-rw-r--r-- 1 olivier olivier 0 Jan 21 10:51 /tmp/sns-sqs-queue-2_1sample_file.zip
```

### Delete aws resources

Delete aws resources (including s3 content !) after the test to avoid unnecessary billing.

```bash
# S3 bucket content will be erased !
terraform destroy
```

## Components details

### S3 bucket

- *Warning* the bucket is fully public, accessible(RW) to anyone.
- send notification to SNS topic on file upload

*When the producer uploads a file to S3, we want the notification to be send only if the file has a specific extension (like a .zip file).*

### SQS queues

One queue per consumer. This permits each consumer to acknowledge(delete) message from the queues independently.

### A SNS topic

Fans out notifications to several SQS queues.

### Publisher sample code

Using python and aws sdk.

- upload  the file to S3

### Consumer sample code

Using python and aws sdk (boto3)

- registers to an SQS queue, and polls the queue until a message appears.
- parses the message to get the location of the file 
- download the file
- delete the message from the queue (to avoid multiple download)

## TODO

- Manage authentication on S3 buckets and queues, limit access to S3 bucket. Only publisher should be granted write on the bucket and consumers should have read only access.
- Manage authentication on upload scripts, use aws roles where applicable
- Test upload / download on big files
- Assess data transfer costs
- Refactor terraform code to ease registering new consumer (avoid duplication of policies a.s.o.)
  
## References

- Using terraform to setup s3, event a.s.o [Event Handling in AWS using SNS, SQS, and Lambda](https://dev.to/frosnerd/event-handling-in-aws-using-sns-sqs-and-lambda-2ng)
- [S3 notifications setup using aws CLI](https://alestic.com/2014/12/s3-bucket-notification-to-sqssns-on-object-creation/)
- [Setting up Monitoring and Alerting on Amazon AWS with Terraform](https://stephenmann.io/post/setting-up-monitoring-and-alerting-on-amazon-aws-with-terraform/)
- [Very good introduction to python and boto3 on AWS](https://realpython.com/python-boto3-aws-s3/)
- [Reading the queue from python](https://github.com/alexandregama/python-sqs-consumer/blob/master/sqs-message-consumer-polling.py)
- AWS doc:[AWS boto3 for python SQS sample tutorial](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/sqs.html)
- AWS doc: [AWS boto3 for python getting started](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html)