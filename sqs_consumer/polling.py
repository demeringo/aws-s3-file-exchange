import argparse
import boto3
import json
import botocore


parser = argparse.ArgumentParser()

parser.add_argument(dest='sqs_queue_name',
                    help='Name of the SQS queue (e.g. sns-sqs-upload)')

args = parser.parse_args()


sqs = boto3.resource('sqs')
s3 = boto3.resource('s3')

# queue = sqs.get_queue_by_name(QueueName='sns-sqs-upload')
queue = sqs.get_queue_by_name(QueueName=args.sqs_queue_name)


while 1:
    messages = queue.receive_messages(WaitTimeSeconds=5)
    for message in messages:

        # msg = json.loads(result.get_body())
        # ret += "Message: %s\n" % msg['message']

        print("Message received: {0}".format(message.body))

        jsonmsgbody = json.loads(message.body)

        print("----: jsonmsg['Message']")
        print(jsonmsgbody['Message']) 
        print("----")
        # print(json.loads(jsonmsg['Message'])['Records'][0]["s3"]["object"]["key"] ) 

        jsonmsgcontent = json.loads(jsonmsgbody['Message'])

        filename = jsonmsgcontent['Records'][0]["s3"]["object"]["key"]
        bucketname = jsonmsgcontent['Records'][0]["s3"]["bucket"]["name"]
        bucketarn =  jsonmsgcontent['Records'][0]["s3"]["bucket"]["arn"]


       # filename = jsonmsg['Message']["Records"][0]["s3"]["object"]["key"] 
        print("File detected: {0} in bucket {1} (bucket arn is: {2})".format(filename, bucketname, bucketarn))
        


        try:
            s3.Bucket(bucketname).download_file(filename, '/tmp/'+args.sqs_queue_name+'_'+filename)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise
        
        #s3.Object(bucketname, filename).download_file(f'/tmp/{filename}') 
        
        message.delete()