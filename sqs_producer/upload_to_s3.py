import argparse
import boto3
import ntpath

parser = argparse.ArgumentParser()

parser.add_argument(dest='filename',
                    help='Name of file to upload')

parser.add_argument(dest='bucket_name',
                    help='Name of the bucket to upload to')

args = parser.parse_args()

# Create an S3 client
s3 = boto3.client('s3')

source_file = args.filename
target_file = ntpath.basename(args.filename)
target_bucket = args.bucket_name

print("Uploading {0} as {1} to bucket {2}".format(source_file, target_file, target_bucket))

# Uploads the given file using a managed uploader, which will split up large
# files automatically and upload parts in parallel.
s3.upload_file(source_file, target_bucket, target_file)